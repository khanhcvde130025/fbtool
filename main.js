const { createNewPost, getUserPage, getPagePost } = require('./util');

/**
 * Simple way to run callback with each element of array synchronous
 * @param {any[]} arr
 * @param {function} callback
 * @returns
 */
const runSynchronous = async (arr, callback) => {
  return arr.reduce((pre, cur) => {
    return pre.then(result => callback({ result, cur }));
  },Promise.resolve()).then(result => callback({ result }));
}

/**
 *
 * @param {*} graph
 * @param {{ pageId: String; pageTokens: String[] }[]} pageData
 * @param {any[]} queuePost
 */
const clonePost = async (graph, pageData, queuePost) => {
  if (!queuePost) {
    return Promise.reject(`Not found any post`);
  }
  return runSynchronous(queuePost, ({ cur: post }) => {
    return post && runSynchronous(pageData, ({ cur }) => {
      const randomToken = cur && cur.pageTokens[Math.floor(Math.random()*cur.pageTokens.length)];
      return cur && createNewPost(graph, randomToken, post).catch(error => console.error(`createNewPost FAIL`, error));
    });
  });
}

/**
 * @param {*} graph
 * @param {string} randomToken
 * @param {string[]} pageIds
 * @param {Number} lastTime
 * @returns {{ data: [], lastTime: Number }} Queue post
 */
const prepareNewPost = async (graph, randomToken, pageIds, lastTime) => {
  const data = [];
  await runSynchronous(pageIds, ({ result, cur }) => {
    if (result) {
      const match = result.filter(post => new Date(post.created_time).getTime()  > lastTime);
      data.push(...(match || []));
    }
    return cur && getPagePost(graph, randomToken, cur).catch(error => console.error('getPagePost FAIL', error));
  });

  return {
    data,
    lastTime: Date.now()
  }
}

/**
 * @param {*} graph
 * @param {String[]} mainPages
 * @param {String[]} adminToken
 * @returns {{ pageId: String; pageTokens: String[] }[]} Array pageId and validate access token
 */
const preparePageToken = async (graph, mainPages, adminToken) => {
  let allPage = [];
  await runSynchronous(adminToken, ({ result, cur }) => {
    if (result) {
      allPage.push(...result);
    }
    return cur && getUserPage(graph, cur).catch(error => console.error('getUserPage FAIL', error));
  })
  return mainPages.reduce((pre, pageId) => {
    const pageTokens = allPage.filter(page => page.id === pageId).map(page => page.access_token);
    if (!pageTokens || !pageTokens.length) {
      return pre;
    }
    pre.push({ pageId, pageTokens });
    return pre;
  }, []);
}

module.exports = {
  clonePost,
  prepareNewPost,
  preparePageToken
}
