module.exports = {

  // Token of user who has admin perm of all pageId, run content of gettoken.js in console.log browser for get token
  adminToken: [
    'EAAAAUaZA8jlABAIgcZAAMmRuhBM0AqOH1ZC9jI6wkSxBovbKFdxXg3djjAKAfeefbwAAdIBbwZBp1ZBr0hsomI4ZC6NLt9LAbP7NrmZCtLwkHsYvs1uZBhzvdoRwDXNWmnpkTdWRtDqgZC56P3xoKO0CwL5oxTZAcovZC0bGSZB5S3PlZBQZDZD',
    'EAAAAUaZA8jlABAIgcZAAMmRuhBM0AqOH1ZC9jI6wkSxBovbKFdxXg3djjAKAfeefbwAAdIBbwZBp1ZBr0hsomI4ZC6NLt9LAbP7NrmZCtLwkHsYvs1uZBhzvdoRwDXNWmnpkTdWRtDqgZC56P3xoKO0CwL5oxTZAcovZC0bGSZB5S3PlZBQZDZD',
    'EAAAAUaZA8jlABAIgcZAAMmRuhBM0AqOH1ZC9jI6wkSxBovbKFdxXg3djjAKAfeefbwAAdIBbwZBp1ZBr0hsomI4ZC6NLt9LAbP7NrmZCtLwkHsYvs1uZBhzvdoRwDXNWmnpkTdWRtDqgZC56P3xoKO0CwL5oxTZAcovZC0bGSZB5S3PlZBQZDZD',
  ],

  // List pageId which you has access control
  mainPages: [
    '315328425977739',
    '315328425977739'
  ],

  // Array list of facebook page Id
  satellitePageIds: [
    '213961285393506',
    '106957926013851',
    '244783322219027',
  ],
}