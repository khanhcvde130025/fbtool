const removeHashTag = message => (message || '').replace(/\#[^\s]*/g, '');

const createNewPost = async (graph, pageToken, { message, attachments }) => {
  const option = {
    message: removeHashTag(message)
  }

  if (!message && !attachments) {
    return Promise.reject('Post not found');
  }

  if (attachments && attachments.data && attachments.data.length) {
    if (attachments.data.some(x => x.media_type === 'link')) {
      return `Post has link, reject`;
    }
    const upAttachment = await uploadAttachments(graph, pageToken, attachments.data).catch(error => console.error('uploadAttachments FAIL', error));

    option.attached_media = upAttachment && upAttachment.map(value => ({ media_fbid: value.id }));
  }

  return new Promise((resolve, reject) => {
    graph.post(`me/feed?access_token=${pageToken}`, option, (err, data) => {
      if (err || !data) {
        return reject({
          err,
          attachments,
          pageToken,
          message
        });
      }
      return resolve({ data });
    });
  })
}

const uploadAttachments = async (graph, pageToken, data) => {
  if (!data || !data.length) {
    return Promise.reject('Not found attachments');
  }

  return Promise.all(data.map(attachment => {
    if (attachment.type === 'photo') {
      return new Promise((resolve, reject) => {
        graph.post(`me/photos?access_token=${pageToken}`, {
          url: attachment.media.image.src,
          published: false,
          caption: removeHashTag(attachment.description)
        }, (err, res) => {
          if (err) {
            return reject({
              attachment,
              err
            });
          }
          return resolve(res);
        });
      })
    }
    if (attachment.type === 'video') {
      return new Promise((resolve, reject) => {
        graph.post(`me/videos?access_token=${pageToken}`, {
          file_url: attachment.media.source,
          published: false,
          caption: removeHashTag(attachment.description)
        }, (err, res) => {
          if (err) {
            return reject({
              attachment,
              err
            });
          }
          return resolve(res);
        });
      })
    }
    if (attachment.type === 'album') {
      return uploadAttachments(graph, pageToken, attachment.subattachments && attachment.subattachments.data);
    }
  }));
}
/**
 *
 *
 * @param {*} graph
 * @param {String} pageToken
 * @param {String} pageId
 * @returns {[*]}
 */
const getPagePost = async (graph, pageToken, pageId) => {
  const fields = [
    'created_time',
    'from',
    'message',
    'parent_id',
    'properties',
    'updated_time',
    'attachments{description,description_tags,media,media_type,title,type,subattachments}'
  ];
  return new Promise((resolve, reject) => {
    graph.get(`${pageId}/feed?fields=${fields.join(',')}&access_token=${pageToken}`, (err, res) => {
      if (err || !res || !res.data) {
        return reject({
          pageToken,
          pageId
        });
      }
      return resolve(res.data);
    })
  })
}

const getUserPage = async (graph, uToken) => {
  return new Promise((resolve, reject) => {
    graph.get(`me/accounts?fields=access_token,name,id&access_token=${uToken}`, (err, res) => {
      if (err || !res || !res.data) {
        return reject({
          uToken,
          err
        });
      }
      return resolve(res.data);
    });
  });
}

module.exports = {
  removeHashTag,
  createNewPost,
  getPagePost,
  getUserPage,
  uploadAttachments
}
