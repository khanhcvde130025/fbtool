const graph = require('fbgraph');
const { adminToken, mainPages, satellitePageIds } = require(`./${process.env.config}.config`);
const { preparePageToken, prepareNewPost, clonePost } = require('./main');

const fiveMinute2Millisecond = 30000000;

let lastPrepareNewPost = Date.now() - fiveMinute2Millisecond;

preparePageToken(graph, mainPages, adminToken).then(pageIdWithTokens => {
  const running = async () => {
    const randomToken = adminToken[Math.floor(Math.random() * adminToken.length)];
    const {
      data,
      lastTime
    } = await prepareNewPost(graph, randomToken, satellitePageIds, lastPrepareNewPost);

    lastPrepareNewPost = lastTime;
    await clonePost(graph, pageIdWithTokens, data);
  };

  running()
  setInterval(running, fiveMinute2Millisecond);
});